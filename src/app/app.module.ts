import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { ReactiveFormsModule } from '@angular/forms';

import { AppComponent } from './app.component';
import { AppRoutingModule } from './app-routing.module';

import { BottomHomeComponent } from './pages/home-page/components/home-container/bottom-home/bottom-home.component';
import { ContentHomeComponent } from './pages/home-page/components/home-container/content-home/content-home.component';
import { HeroHomeComponent } from './pages/home-page/components/home-container/hero-home/hero-home.component';
import { HomeContainerComponent } from './pages/home-page/components/home-container/home-container.component';

import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';

@NgModule({
    declarations: [AppComponent, HomeContainerComponent, HeroHomeComponent, ContentHomeComponent, BottomHomeComponent],
    imports: [BrowserModule, AppRoutingModule, FontAwesomeModule, ReactiveFormsModule],
    providers: [],
    bootstrap: [AppComponent],
})
export class AppModule {}
