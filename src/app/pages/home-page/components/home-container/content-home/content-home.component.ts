import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { UserRegister } from 'src/app/pages/home-page/models/Ihome.model';
import { comparePassword } from './customValidator';

@Component({
    selector: 'app-content-home',
    templateUrl: './content-home.component.html',
    styleUrls: ['./content-home.component.scss'],
})
export class ContentHomeComponent implements OnInit {
    // Form initialization
    public userRegisterForm: FormGroup = new FormGroup({});
    // variable submitted to false
    public submitted: boolean = false;
    constructor(private formBuilder: FormBuilder) {}

    ngOnInit(): void {
        // Our form without fields  by default
        // We can put default values between the ''
        this.userRegisterForm = this.formBuilder.group(
            {
                name: ['', [Validators.required, Validators.maxLength(20)]],
                password: ['', [Validators.required, Validators.pattern(/^(?=.*\d)(?=.*[a-z])(?=.*[A-Z])(?=.*[a-zA-Z]).{8,}$/)]],
                passwordRepeat: ['', [Validators.required, Validators.pattern(/^(?=.*\d)(?=.*[a-z])(?=.*[A-Z])(?=.*[a-zA-Z]).{8,}$/)]],
            },
            { validator: comparePassword('password', 'passwordRepeat') }
        );
    }

    onSubmit(): void {
        this.submitted = true;
        // If form is valid
        if (this.userRegisterForm.valid) {
            setTimeout(() => {
                // We create and send user
                const user: UserRegister = {
                    name: this.userRegisterForm.get('name')?.value,
                    password: this.userRegisterForm.get('password')?.value,
                    passwordRepeat: this.userRegisterForm.get('passwordRepeat')?.value,
                    isActivate: false,
                };
                // Reset all fields and submitted
                this.userRegisterForm.reset();
                this.submitted = false;
            }, 3000);
        }
    }
}
