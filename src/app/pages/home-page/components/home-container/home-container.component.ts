import { Component, OnInit } from '@angular/core';
import { faCommentDots } from '@fortawesome/free-solid-svg-icons';

@Component({
    selector: 'app-home-container',
    templateUrl: './home-container.component.html',
    styleUrls: ['./home-container.component.scss'],
})
export class HomeContainerComponent implements OnInit {
    faCommentDots = faCommentDots;
    constructor() {}

    ngOnInit(): void {}
}
