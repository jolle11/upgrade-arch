export interface UserRegister {
    name: string;
    password: string;
    passwordRepeat: string;
    isActivate: boolean;
}
